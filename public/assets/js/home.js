$(document).ready(function(){

	// Master Slider
	var slider = new MasterSlider();
		
	slider.control('arrows');	
	slider.control('thumblist' , {autohide:false ,dir:'h',arrows:false, align:'bottom', width:127, height:70, margin:5, space:5});
	slider.setup('masterslider' , {
		width:850,
		height:338,
		space:5,
		view:'basic'
	});

	//SLider banner
	$('.owl-banners').owlCarousel({
	    loop:true,
	    margin:00,
	    nav:false,
	    responsive:{
	        0:{
	            items:1
	        },
	        600:{
	            items:1
	        },
	        1000:{
	            items:1
	        }
	    }
	});


	//Slider destacadas
	var owl = $('.owl-featured');
	owl.owlCarousel({
		loop:false,
	    margin:0,
	    nav:false,
	    responsive:{
	        0:{
	            items:1
	        },
	        600:{
	            items:1
	        },
	        1000:{
	            items:1
	        }
	    }
	});

	$('.next').click(function() {
	    owl.trigger('next.owl.carousel');
	})
	// Go to the previous item
	$('.prev').click(function() {
	    owl.trigger('prev.owl.carousel');
	});

	//Slider destacadas
	var owl = $('.owl-featured-video');
	owl.owlCarousel({
		loop:false,
	    margin:0,
	    nav:false,
	    responsive:{
	        0:{
	            items:1
	        },
	        600:{
	            items:1
	        },
	        1000:{
	            items:1
	        }
	    }
	});

	$('.next1').click(function() {
	    owl.trigger('next.owl.carousel');
	})
	// Go to the previous item
	$('.prev1').click(function() {
	    owl.trigger('prev.owl.carousel');
	});

});