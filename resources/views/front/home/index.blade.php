@extends('front.layouts.layout')

@section('title')
	HomePage
@endsection

@section('css')
	<link rel="stylesheet" href="/assets/css/home.css">
	<link rel="stylesheet" href="/assets/plugins/master-slider/style/masterslider.css">
	<link rel="stylesheet" href="/assets/plugins/master-slider/ms-showcase2.css">
	<link rel="stylesheet" href="/assets/plugins/master-slider/skins/light-5/style.css">
	<link rel="stylesheet" href="/assets/plugins/owl-slider/assets/owl.carousel.min.css">
	<link rel="stylesheet" href="/assets/plugins/owl-slider/assets/owl.theme.default.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
@endsection

@section('bodyId')
@endsection

@section('body')
	<div class="container space">
		<div class="row">
			<div class="col-xs-12">
				<div class="col-xs-8">

					<div class="ms-showcase2-template">
						<div class="master-slider ms-skin-default" id="masterslider">

							@for ($i = 0; $i < 10 ; $i++)
						    <div class="ms-slide container-info">
						    	<img src="/assets/plugins/master-slider/blank.gif" data-src="https://www.elcuartodeguerra.com/images/stories/2017/Diciembre/11Diciembre/11_slide.jpg" alt="lorem ipsum dolor sit"/>
					    		<div class="info">
					    			<h3>Inaplazable la aprobación de la ley de seguridad interior:</h3>
					    			<p>Durante su participación en la Reunión de los Presidentes de las Comisiones Dictaminadoras de la Minuta a esta Ley con Gobernadores y Alcaldes, Marco Mena puntualizó que, al mismo tiempo.</p>
					    		</div>
						        <img class="ms-thumb" src="https://www.elcuartodeguerra.com/images/stories/2017/Diciembre/11Diciembre/11_slide.jpg" alt="thumb" />
						    </div>
						    @endfor
						</div>
					</div>

					<div class="col-xs-12">
						<div class="row featured">
							<div class="title-featured v-align">
								<div class="col-xs-10 sinpadding">
									<h3>Noticias destacadas minuto a minuto</h3>
								</div>
								<div class="col-xs-2 sinpadding">
									<div class="btn-group buttons-featured">
									  <button type="button" class="btn btn-default prev">
									  	<i class="fa fa-angle-left" aria-hidden="true"></i>
									  </button>
									  <button type="button" class="btn btn-default next">
									  	<i class="fa fa-angle-right" aria-hidden="true"></i>
									  </button>
									</div>
								</div>
							</div>
							<div class="clearfix"></div>
							<div class="owl-carousel owl-featured">
								@for ($o = 0; $o < 4; $o++)
								
								<div class="item">

									<div class="col-xs-7 sinpadding">
										<a href="#">
											<div class="featured-item">
												<img src="https://www.elcuartodeguerra.com/images/stories/2017/Diciembre/11Diciembre/11_slide.jpg" alt="" class="img-responsive">
												<h3>Se reúnen especialistas en biotecnología y biodiversidad en la UAT</h3>
												<div class="date">04 Diciembre 2017</div>
												<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem eaque minima esse non facilis, tempora.</p>
											</div>
										</a>
									</div>
									<div class="col-xs-5">
										@for ($i = 0; $i < 5; $i++)
										<a href="#">
											<div class="featured-mini">
												<div class="col-xs-5">
													<div class="img" style="background-image:url('https://www.elcuartodeguerra.com/images/stories/2017/Diciembre/11Diciembre/11_slide.jpg');"></div>
												</div>
												<div class="col-xs-7 sinpadding">
													<h3>Mujer intentaba abordar un autobús con su hijo muerto.</h3>
												</div>
												<div class="clearfix"></div>
											</div>
										</a>
										@endfor
									</div>

								</div>
								@endfor

							</div>
						</div>
					</div>

				</div>
				<div class="col-xs-4">

					<div class="owl-carousel owl-banners owl-theme">
    					<div class="item" style="background-image:url('http://coderflex.com/cgh/images/promo%203.jpg?crc=3847883161');">
    					</div>
    				</div>

    				<div class="col-xs-12" id="relevant">

    					@for ($i = 0; $i < 6; $i++)
    					<a href="#{{$i}}">
	    					<div class="col-xs-6 relevant-item sinpadding">
	    						<div class="img" style="background-image:url('http://coderflex.com/cgh/images/promo%203.jpg?crc=3847883161');"></div>
	    						<h3>Reconoce cámara de diputados a Tlaxcala por eliminación de VIH...</h3>
	    					</div>
    					</a>
    					@endfor

    				</div>

				</div>
			</div>
		</div>
	</div>

	{{-- Banners --}}
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="col-xs-12" id="banner-full">
					<div class="img-shadow">
						<img src="/assets/img/home/banner.jpg" alt="#" class="img-responsive">
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="col-xs-8">

					<div class="col-xs-12">
						<div class="row featured2">
							<div class="title-featured v-align">
								<div class="col-xs-10 sinpadding">
									<h3>Video notas destacadas</h3>
								</div>
								<div class="col-xs-2 sinpadding">
									<div class="btn-group buttons-featured">
									  <button type="button" class="btn btn-default prev1">
									  	<i class="fa fa-angle-left" aria-hidden="true"></i>
									  </button>
									  <button type="button" class="btn btn-default next1">
									  	<i class="fa fa-angle-right" aria-hidden="true"></i>
									  </button>
									</div>
								</div>
							</div>
							<div class="clearfix"></div>
							<div class="owl-carousel owl-featured-video">
								@for ($o = 0; $o < 4; $o++)
								
								<div class="item">

									<div class="col-xs-7 sinpadding">
										<a href="#">
											<div class="featured-item">
												<img src="https://www.elcuartodeguerra.com/images/stories/2017/Diciembre/11Diciembre/11_slide.jpg" alt="" class="img-responsive">
												<h3>Se reúnen especialistas en biotecnología y biodiversidad en la UAT</h3>
												<div class="date">04 Diciembre 2017</div>
												<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem eaque minima esse non facilis, tempora.</p>
											</div>
										</a>
									</div>
									<div class="col-xs-5">
										@for ($i = 0; $i < 5; $i++)
										<a href="#">
											<div class="featured-mini">
												<div class="col-xs-5">
													<div class="img" style="background-image:url('https://www.elcuartodeguerra.com/images/stories/2017/Diciembre/11Diciembre/11_slide.jpg');"></div>
												</div>
												<div class="col-xs-7 sinpadding">
													<h3>Mujer intentaba abordar un autobús con su hijo muerto.</h3>
												</div>
												<div class="clearfix"></div>
											</div>
										</a>
										@endfor
									</div>

								</div>
								@endfor

							</div>
						</div>
					</div>

				</div>
				<div class="col-xs-4">					

					<div class="col-xs-12" id="relevant2">

						@for ($i = 0; $i < 6; $i++)
						<a href="#{{$i}}">
	    					<div class="col-xs-6 relevant-item sinpadding">
	    						<div class="img" style="background-image:url('http://coderflex.com/cgh/images/promo%203.jpg?crc=3847883161');"></div>
	    						<h3>Reconoce cámara de diputados a Tlaxcala por eliminación de VIH...</h3>
	    					</div>
						</a>
						@endfor

					</div>

				</div>
			</div>
		</div>
	</div>
	<div class="container" id="banners-four">
		<div class="row">
			<div class="col-xs-12">
				<div class="col-xs-3">
					<div class="shadow">
						<img src="http://coderflex.com/cgh/images/promo%203.jpg?crc=3847883161" alt="#" class="img-responsive">
					</div>
				</div>
				<div class="col-xs-3">
					<div class="shadow">
						<img src="http://coderflex.com/cgh/images/promo%203.jpg?crc=3847883161" alt="#" class="img-responsive">
					</div>
				</div>
				<div class="col-xs-3">
					<div class="shadow">
						<img src="http://coderflex.com/cgh/images/promo%203.jpg?crc=3847883161" alt="#" class="img-responsive">
					</div>
				</div>
				<div class="col-xs-3">
					<div class="shadow">
						<img src="http://coderflex.com/cgh/images/promo%203.jpg?crc=3847883161" alt="#" class="img-responsive">
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="col-xs-12">
			<div class="col-xs-8">
				<div class="row featured3">
					<div class="title-featured v-align">
						<div class="col-xs-10 sinpadding">
							<h3>Columnistas</h3>
						</div>
						<div class="col-xs-2 sinpadding">
							<div class="btn-group buttons-featured">
							  <button type="button" class="btn btn-default prev">
							  	<i class="fa fa-angle-left" aria-hidden="true"></i>
							  </button>
							  <button type="button" class="btn btn-default next">
							  	<i class="fa fa-angle-right" aria-hidden="true"></i>
							  </button>
							</div>
						</div>
					</div>
					<div class="clearfix"></div>
					<div class="owl-carousel owl-featured">
						@for ($o = 0; $o < 4; $o++)
						
						<div class="item">

							<div class="col-xs-7 sinpadding">
								<a href="#">
									<div class="featured-item">
										<img src="https://www.elcuartodeguerra.com/images/stories/2017/Diciembre/11Diciembre/11_slide.jpg" alt="" class="img-responsive">
										<h3>Se reúnen especialistas en biotecnología y biodiversidad en la UAT</h3>
										<div class="date">04 Diciembre 2017</div>
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem eaque minima esse non facilis, tempora.</p>
									</div>
								</a>
							</div>
							<div class="col-xs-5">
								@for ($i = 0; $i < 5; $i++)
								<a href="#">
									<div class="featured-mini">
										<div class="col-xs-5">
											<div class="img" style="background-image:url('https://www.elcuartodeguerra.com/images/stories/2017/Diciembre/11Diciembre/11_slide.jpg');"></div>
										</div>
										<div class="col-xs-7 sinpadding">
											<h3>Mujer intentaba abordar un autobús con su hijo muerto.</h3>
										</div>
										<div class="clearfix"></div>
									</div>
								</a>
								@endfor
							</div>

						</div>
						@endfor

					</div>
				</div>
			</div>
			<div class="col-xs-4">
			
				<div class="col-xs-12" id="relevant2">
					@for ($i = 0; $i < 6; $i++)
					<a href="#{{$i}}">
						<div class="col-xs-6 relevant-item sinpadding">
							<div class="img" style="background-image:url('http://coderflex.com/cgh/images/promo%203.jpg?crc=3847883161');"></div>
							<h3>Reconoce cámara de diputados a Tlaxcala por eliminación de VIH...</h3>
						</div>
					</a>
					@endfor

				</div>
			</div>
		</div>
	</div>
@endsection

@section('js')
	<script src="/assets/plugins/master-slider/masterslider.min.js"></script>
	<script src="/assets/plugins/owl-slider/owl.carousel.min.js"></script>
	<script type="text/javascript" src="/assets/js/home.js"></script>
@endsection