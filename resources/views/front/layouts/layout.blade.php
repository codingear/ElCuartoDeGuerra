<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>@yield('title')</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
		<link rel="stylesheet" href="/assets/css/main.css">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
		@yield('css')
	</head>
	<body id="@yield('bodyId')">

		<section id="navbar-top">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<div class="col-xs-2">
							<img src="/assets/img/brand/logo-top.svg" alt="El cuarto de guerra" class="img-responsive logo">
						</div>
						<div class="col-xs-6"></div>
						<div class="col-xs-4"></div>
					</div>
				</div>
			</div>
		</section>

		<section id="navbar-sup">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<div class="col-xs-5">
							<img src="/assets/img/brand/logo-cg.svg" alt="El cuarto de guerra" class="img-responsive">
						</div>
						<div class="col-xs-4 col-xs-offset-3"></div>
					</div>
				</div>
			</div>
		</section>

		<section id="container-menu">
			<section id="navbar-menu">
				<nav class="navbar navbar-default" role="navigation">
					<div class="container">
						<!-- Brand and toggle get grouped for better mobile display -->
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>
				
						<!-- Collect the nav links, forms, and other content for toggling -->
						<div class="collapse navbar-collapse navbar-ex1-collapse">
							<ul class="nav navbar-nav">
								<li><a href="#">Portada</a></li>
								<li><a href="#">Directorio</a></li>
								<li><a href="#">Nosotros</a></li>
								<li><a href="#">Columnistas</a></li>
								<li><a href="#">Contacto</a></li>
							</ul>
							<form class="navbar-form navbar-right" role="search">
								<div class="form-group">
									<input type="text" class="form-control" placeholder="Search">
								</div>
								<button type="submit" class="btn btn-default">Buscar</button>
							</form>
						</div><!-- /.navbar-collapse -->
					</div>
				</nav>
			</section>

			<section id="navbar-submenu">
				<nav class="navbar navbar-default" role="navigation">
					<div class="container">
						{{-- <div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div> --}}
				
						<!-- Collect the nav links, forms, and other content for toggling -->
						<div class="collapse navbar-collapse navbar-ex1-collapse">
							<ul class="nav navbar-nav">
								<li>
									<div class="g"><img src="/assets/img/brand/cg-g.png" alt="" class="img-responsive"></div>
										<a href="#"> Tlaxcala</a>
								</li>
								<li>
									<div class="g"><img src="/assets/img/brand/cg-g.png" alt="" class="img-responsive"></div>
									<a href=""> Apizaco</a>
								</li>
								<li>
									<div class="g"><img src="/assets/img/brand/cg-g.png" alt="" class="img-responsive"></div>
									<a href=""> Chiautempan</a>
								</li>
								<li>
									<div class="g"><img src="/assets/img/brand/cg-g.png" alt="" class="img-responsive"></div>
									<a href=""> Galpaulpan</a>
								</li>
								<li>
									<div class="g"><img src="/assets/img/brand/cg-g.png" alt="" class="img-responsive"></div>
									<a href=""> Zacatelco</a>
								</li>
								<li>
									<div class="g"><img src="/assets/img/brand/cg-g.png" alt="" class="img-responsive"></div>
									<a href=""> Puebla</a>
								</li>
							</ul>
						</div><!-- /.navbar-collapse -->
					</div>
				</nav>
			</section>
		</section>

		@yield('body')
		
		<script src="//code.jquery.com/jquery.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
		@yield('js')
	</body>
</html>